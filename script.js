(function(window, document) {

    const elementIds = {
        button: "id_searchOpenaire",
        entityType: "id_type",
        keywords: "id_keywords",
        author: "id_author",
        fp7scientificarea: "id_fp7scientificarea",
        acceptedDate: "id_acceptedDate",
        fromDateAccepted: "id_fromDateAccepted_year",
        toDateAccepted: "id_toDateAccepted_year",
    };

    const selectedSearchResults = {};

    function getSearchParams() {
        // grab various elements with search input, get their values
        const entityType = document.getElementById(elementIds.entityType).value;
        const keywords = document.getElementById(elementIds.keywords).value;
        const author = document.getElementById(elementIds.author).value;
        const fp7scientificarea = document.getElementById(elementIds.fp7scientificarea).value;

        if (document.getElementById(elementIds.acceptedDate).value) {
            const fromDateAccepted = document.getElementById(elementIds.fromDateAccepted).value;
            const toDateAccepted = document.getElementById(elementIds.toDateAccepted).value;
        }

        return {
            entityType: entityType,
            keywords: keywords,
            author: author, 
            fp7scientificarea: fp7scientificarea, 
            //fromDateAccepted: fromDateAccepted, 
            //toDateAccepted: toDateAccepted, 
        };
    }

    function searchOpenAire(searchParams) {
        // send HTTP request to openaire, return data results
        return fetch(`http://api.openaire.eu/search/${searchParams.entityType}?format=json&keywords=${searchParams.keywords}`)
            .then(response => response.json())
            .then(data => {
                return data.response.results.result.map((r) => {
                    const id = r.header['dri:objIdentifier'].$;

                    let openaireurl = "https://explore.openaire.eu/search/";

                    if (searchParams.entityType == "publications") {
                        openaireurl = "https://explore.openaire.eu/search/publication?articleId="
                    } else if (searchParams.entityType == "software") {
                        openaireurl = "https://explore.openaire.eu/search/software?softwareId="
                    } else if  (searchParams.entityType == "datasets") {
                        openaireurl = "https://explore.openaire.eu/search/dataset?datasetId="
                    } else if  (searchParams.entityType == "other") {
                        openaireurl = "https://explore.openaire.eu/search/other?orpId="
                    }

                    let pubtitle = r.metadata['oaf:entity']['oaf:result'].title.$;
                    if (pubtitle == null) {
                        try{
                            pubtitle = r.metadata['oaf:entity']['oaf:result']['title'][0].$;
                        }
                        catch(error){
                            pubtitle = $openaireurl + id;
                        }
                    }

                    let pubdate = r.metadata['oaf:entity']['oaf:result'].dateofacceptance.$;

                    let pubcreator = r.metadata['oaf:entity']['oaf:result'].creator.$;
                    if (pubcreator == null) {
                        pubcreator = "";
                        try{
                            let pubcreatorlist = r.metadata['oaf:entity']['oaf:result'].creator;
                            var i;
                            for (i = 0; i < pubcreatorlist.length; i++) {
                                pubcreator += pubcreatorlist[i].$ + ";";
                              }
                        }
                        catch(error){
                            pubcreator = "";
                        }
                    }

                    return {
                        url: openaireurl + id,
                        id: id,
						title: pubtitle + " (" + pubdate.substring(0, 4) + ") \r\n" + pubcreator

                    }
                });
            });
    }

    function fillFormFromSearchResults(results) {
        // fill form elements with search results

        if (document.getElementById("openairesearchresults") !== null) {
            document.getElementById("openairesearchresults").remove();
        }

        const buttonContainerElem = document.getElementById("fitem_id_searchOpenaire");
        const resultContainer = document.createElement("div");
        resultContainer.setAttribute("id", "openairesearchresults");
        

        const resultHeader = document.createElement("div");
        resultHeader.textContent = "OpenAIRE Search Results";
        resultContainer.appendChild(resultHeader);  

        // XXX: each time a checkbox is checked, fill the value
        // <input type="url" class="form-control " name="externalurl" id="id_externalurl" value="" size="60">
        // <input name="externalurl" type="hidden">
        
        for (const result of results) {

            const {id, url, title} = result;

            const checkboxgrid = document.createElement("div");
            checkboxgrid.setAttribute("id", "checkboxgrid");
            checkboxgrid.setAttribute("class", "grid-container");
            checkboxgrid.setAttribute("style", "display: grid; grid-column-gap: 50px; grid-template-columns: 5px auto; padding: 10px;");

            const checkboxgriditem = document.createElement("div");
            checkboxgriditem.setAttribute("id", "checkboxgriditem");
            checkboxgriditem.setAttribute("class", "grid-item");

            const checkbox = document.createElement("input");
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("name", "openaire_result_checked_" + id);
            checkbox.setAttribute("class", "form-check");

            checkbox.addEventListener('change', () => {
                if(checkbox.checked) {
                    // let id = checkbox.getAttribute("name").split("openaire_result_checked_")[1];
                    // let link = document.getElementById("openaire_result_" + id);
                    // selectedSearchResults[id] = {id, url: link.getAttribute("href"), title: link.textContent};
                    selectedSearchResults[id] = {id, url: url, title: title};
                    console.log("selectedSearchResults", selectedSearchResults)
                } else {
                    delete selectedSearchResults[id];
                }
              });
            
            checkboxgriditem.appendChild(checkbox);
            
            const linkgriditem = document.createElement("div");
            linkgriditem.setAttribute("id", "linkgriditem_" + id);
            linkgriditem.setAttribute("class", "grid-item");
            linkgriditem.setAttribute("style", "text-align: left;");
            const link = document.createElement("a");
            link.setAttribute("href", url);
            link.setAttribute("target", "_blank");
            link.setAttribute("id", "openaire_result_" + id);
            
            link.setAttribute('style', 'white-space: pre;');
            link.textContent = title;
            
            linkgriditem.appendChild(link);

            checkboxgrid.appendChild(checkboxgriditem);
            checkboxgrid.appendChild(linkgriditem);
            resultContainer.appendChild(checkboxgrid);

        }

        buttonContainerElem.parentElement.appendChild(resultContainer);  
    }

    function setup() {
        document.getElementById(elementIds.button).onclick = () => {
            const searchParams = getSearchParams();
            searchOpenAire(searchParams).then((results) => {
                fillFormFromSearchResults(results)
            }).catch(err => {
                console.error("search failed with error", err)
            })
        }

        document.getElementsByTagName("form")[0].onsubmit = () => {
            const hiddeninput = document.getElementById("selected_search_results_value")
            console.log("selectedSearchResults", selectedSearchResults);
            hiddeninput.value = JSON.stringify(Object.values(selectedSearchResults));
        }

        // <input name="externalurl" type="hidden">
        const hiddeninput = document.createElement("input");
        hiddeninput.setAttribute("name", "externalurl");
        hiddeninput.setAttribute("type", "hidden");
        hiddeninput.setAttribute("id", "selected_search_results_value");
        document.getElementById("fitem_id_searchOpenaire").parentElement.appendChild(hiddeninput);  
    }

    window.addEventListener('DOMContentLoaded', (event) => {
        console.log('Setting up search')
        setup();
    });

})(window, document);
